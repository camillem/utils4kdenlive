# SPDX-FileCopyrightText: 2021 CamilleM
# SPDX-License-Identifier: WTFPL

# A simple script to parse C++ headers


import os

ignorable_extensions = ["png", "gif", "jpeg", "ui", "rc", "qrc", "kcfg", "kcfgc", "xml"]
files_to_ignore = ["CMakeLists.txt"]
# Set the directory you want to start from
rootDir = "src"
headerless = 0
for dir_name, subdirList, fileList in os.walk(rootDir):
    #    print("Found directory: %s" % dir_name)
    for fname in fileList:
        if fname not in files_to_ignore:
            try:
                extension = fname.split(".")[1]
            except:
                extension = ""
            if extension not in ignorable_extensions:
                #            print("\t%s" % fname)
                file = open(os.path.join(dir_name, fname), "r")
                header = ""
                finished = False
                inside_header = False
                while not finished:
                    line = file.readline()
                    if not line or "*/" in line:
                        finished = True
                        header += line
                    elif "/*" in line or inside_header:
                        header += line
                        inside_header = True
                #            print(header)
                if not header:
                    headerless += 1
                #    print("[-]%s has no header" % os.path.join(dir_name, fname))
                else :

                #                else:
                #                    print("[+]")
                file.close()
print(headerless, "fichiers sans en-tête")
