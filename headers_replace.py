# SPDX-FileCopyrightText: 2021 CamilleM
# SPDX-License-Identifier: WTFPL

# A simple script to parse C++ headers

import os

replacements = [
    [
        """/***************************************************************************
 *   Copyright (C) 2019 by Nicolas Carion                                  *
 *   This file is part of Kdenlive. See www.kdenlive.org.                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) version 3 or any later version accepted by the       *
 *   membership of KDE e.V. (or its successor approved  by the membership  *
 *   of KDE e.V.), which shall act as a proxy defined in Section 14 of     *
 *   version 3 of the license.                                             *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 ***************************************************************************/""",
        """/*
    SPDX-FileCopyrightText: 2019 Nicolas Carion
    SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL
*/""",
    ],
    [
        """ *   This file is part of Kdenlive. See www.kdenlive.org.                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) version 3 or any later version accepted by the       *
 *   membership of KDE e.V. (or its successor approved  by the membership  *
 *   of KDE e.V.), which shall act as a proxy defined in Section 14 of     *
 *   version 3 of the license.                                             *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 ***************************************************************************/""",
        """ * SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL
 */""",
    ],
    [
        """/***************************************************************************""",
        """/*""",
    ],
    [
        """SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL""",
        """SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL""",
    ],
    [
        """ *   SPDX-License-Identifier: GPL-2.0-or-later""",
        """SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL""",
    ],
    [
        """ *   This file is part of Kdenlive (www.kdenlive.org).                     *
 *                                                                         *
 *   Kdenlive is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Kdenlive is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Kdenlive.  If not, see <http://www.gnu.org/licenses/>.     *
 ***************************************************************************/""",
        """ SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL
 */""",
    ],
    [
        """This file is part of kdenlive. See www.kdenlive.org.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
*/""",
        """SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL
*/""",
    ],
    [
        """ *   This file is part of Kdenlive. See www.kdenlive.org.                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) version 3 or any later version accepted by the       *
 *   membership of KDE e.V. (or its successor approved  by the membership  *
 *   of KDE e.V.), which shall act as a proxy defined in Section 14 of     *
 *   version 3 of the license.                                             *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/""",
        """ SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL
 */""",
    ],
]


ignorable_extensions = [
    "png",
    "gif",
    "jpeg",
    "ui",
    "rc",
    "qrc",
    "kcfg",
    "kcfgc",
    "mkv",
    "ico",
    "svgz",
    "svg",
    "pgm",
]
files_to_ignore = ["CMakeLists.txt"]
# Set the directory you want to start from
rootDir = "../kdenlive/src"
substitutions = 0
for dir_name, subdirList, fileList in os.walk(rootDir):
    print("Found directory: %s" % dir_name)
    for fname in fileList:
        if fname not in files_to_ignore:
            try:
                extension = fname.split(".")[1]
            except:
                extension = ""
            if extension not in ignorable_extensions:
                #            print("\t%s" % fname)
                current_file = open(os.path.join(dir_name, fname), "r+")
                content = current_file.read()
                current_file.seek(0)
                for replacement in replacements:
                    content = content.replace(replacement[0], replacement[1])
                current_file.write(content)
                current_file.truncate()
                current_file.close()
print(substitutions, "substitutions operated")
